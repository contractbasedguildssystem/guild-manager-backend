package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Clause;
import dev.guilds.manager.backend.model.meta.PotentatiallySignable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PotentialContractIntern implements PotentatiallySignable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_potential_contract_intern_generator")
    @SequenceGenerator(name = "id_potential_contract_intern_generator", sequenceName = "id_potential_contract_intern_generator_sequence", allocationSize = 1)
    private Long id;

    @ManyToMany(mappedBy = "potentialContractInterns")
    private List<GuildDev> guildDevs = new ArrayList<>();

    @OneToMany(mappedBy = "potentialContract")
    private List<PotentialClause> potentialClauses = new ArrayList<>();

    public PotentialContractIntern() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<GuildDev> getGuildDevs() {
        return guildDevs;
    }

    public void setGuildDevs(List<GuildDev> signers) {
        this.guildDevs = signers;
    }

    @Override
    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PotentialContractIntern that)) return false;

        if (!guildDevs.equals(that.guildDevs)) return false;
        return clauses.equals(that.clauses);
    }

    @Override
    public int hashCode() {
        int result = guildDevs.hashCode();
        result = 31 * result + clauses.hashCode();
        return result;
    }
}

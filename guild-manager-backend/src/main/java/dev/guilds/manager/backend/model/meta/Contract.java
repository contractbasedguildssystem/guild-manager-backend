package dev.guilds.manager.backend.model.meta;

import java.util.List;

public interface Contract {

    List<? extends Signer> getSigners();

    List<Clause> getClauses();
}

package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Signer;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class NonDev extends Signer {

    /**
     *
     * Purely technical id
     * No solution to authenticate an NonDevPerson.
     * We are only sure of each other.
     * This object can even be a Morale Person ! Like in enterprise.
     * Anyone who is not Dev nor in our Guild and who can sign contracts.
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_person_generator")
    @SequenceGenerator(name = "id_person_generator", sequenceName = "id_person_generator_sequence", allocationSize = 1)
    private Long id;

    /**
     *
     * This field should be updatable.
     * We should recognize name-changes for non-devs or for devs from other guilds' members.
     *
     */
    @Column(nullable = false)
    private String firstName;

    @Column(updatable = false)
    private String secondFirstName;

    @Column(updatable = false)
    private String thirdFirstName;

    @Column(nullable = false)
    private String lastName;

    public NonDev() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondFirstName() {
        return secondFirstName;
    }

    public void setSecondFirstName(String secondFirstName) {
        this.secondFirstName = secondFirstName;
    }

    public String getThirdFirstName() {
        return thirdFirstName;
    }

    public void setThirdFirstName(String thirdFirstName) {
        this.thirdFirstName = thirdFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NonDev that)) return false;

        if (!firstName.equals(that.firstName)) return false;
        if (!Objects.equals(secondFirstName, that.secondFirstName))
            return false;
        if (!Objects.equals(thirdFirstName, that.thirdFirstName))
            return false;
        return lastName.equals(that.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + (secondFirstName != null ? secondFirstName.hashCode() : 0);
        result = 31 * result + (thirdFirstName != null ? thirdFirstName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        return result;
    }
}

package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Signer;

import javax.persistence.*;
import java.util.Objects;

// An Apprentice is a Dev in no Guild trying to join ours.
// He will have to be validated in some way or another.
public class Apprentice extends Signer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_apprentice_generator")
    @SequenceGenerator(name = "id_apprentice_generator", sequenceName = "id_apprentice_generator_sequence", allocationSize = 1)
    private Long id;

    @Column(nullable = false, updatable = false)
    private String firstName;

    @Column(updatable = false)
    private String secondFirstName;

    @Column(nullable = false, updatable = false)
    private String lastName;

    public Apprentice() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondFirstName() {
        return secondFirstName;
    }

    public void setSecondFirstName(String secondFirstName) {
        this.secondFirstName = secondFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Apprentice that)) return false;

        if (!firstName.equals(that.firstName)) return false;
        if (!Objects.equals(secondFirstName, that.secondFirstName))
            return false;
        return lastName.equals(that.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + (secondFirstName != null ? secondFirstName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        return result;
    }
}

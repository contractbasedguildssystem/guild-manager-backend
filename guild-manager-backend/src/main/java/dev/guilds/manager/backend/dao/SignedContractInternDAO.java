package dev.guilds.manager.backend.dao;

import dev.guilds.manager.backend.model.SignedContractIntern;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RequestScoped
@Transactional
public class SignedContractInternDAO {

    @Inject
    EntityManager em;

    public SignedContractIntern create(SignedContractIntern signedContractIntern) {
        em.persist(signedContractIntern);
        em.flush();
        return signedContractIntern;
    }
}

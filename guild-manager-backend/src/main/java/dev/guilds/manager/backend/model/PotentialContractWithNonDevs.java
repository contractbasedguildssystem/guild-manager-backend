package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Clause;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PotentialContractWithNonDevs {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_potential_contract_with_non_devs_generator")
    @SequenceGenerator(name = "id_potential_contract_with_non_devs_generator", sequenceName = "id_potential_contract_with_non_devs_generator_sequence", allocationSize = 1)
    private Long id;

    @ManyToMany(mappedBy = "potentialContracts")
    private List<GuildDev> ourSigners = new ArrayList<>();

    @ManyToMany(mappedBy = "potentialContracts")
    private List<NonDev> nonDevSigners = new ArrayList<>();

    @OneToMany(mappedBy = "potentialContract")
    private List<Clause> clauses = new ArrayList<>();

    public PotentialContractWithNonDevs() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<GuildDev> getOurSigners() {
        return ourSigners;
    }

    public void setOurSigners(List<GuildDev> ourSigners) {
        this.ourSigners = ourSigners;
    }

    public List<NonDev> getNonDevSigners() {
        return nonDevSigners;
    }

    public void setNonDevSigners(List<NonDev> nonDevSigners) {
        this.nonDevSigners = nonDevSigners;
    }

    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PotentialContractWithNonDevs that)) return false;

        if (!ourSigners.equals(that.ourSigners)) return false;
        if (!nonDevSigners.equals(that.nonDevSigners)) return false;
        return clauses.equals(that.clauses);
    }

    @Override
    public int hashCode() {
        int result = ourSigners.hashCode();
        result = 31 * result + nonDevSigners.hashCode();
        result = 31 * result + clauses.hashCode();
        return result;
    }
}

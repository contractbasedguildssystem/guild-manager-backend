package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Signer;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class GuildDev extends Signer {

    /**
     *
     * This id is purely technical !!!
     * A DevPerson Entity from this Guild can be saved in another instance !
     * We should only use the firstNames, lastName to identify !
     * All our ids are only ours ! So only GuildDev ids are true to us.
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_dev_generator")
    @SequenceGenerator(name = "id_dev_generator", sequenceName = "id_dev_generator_sequence", allocationSize = 1)
    private Long id;

    @Column(updatable = false, nullable = false)
    private String firstName;

    @Column(updatable = false)
    private String secondFirstName;

    @Column(updatable = false)
    private String thirdFirstName;

    @Column(updatable = false)
    private String fourthFirstName;

    @Column(updatable = false)
    private String fifthFirstName;

    @Column(updatable = false, nullable = false)
    private String lastName;

    @ManyToMany(mappedBy = "guildDevs")
    private List<PotentialContractIntern> potentialContractInterns = new ArrayList<>();

    @ManyToMany(mappedBy = "guildDevs")
    private List<PotentialContractWithDevs> potentialContractWithDevs = new ArrayList<>();

    @ManyToMany(mappedBy = "")
    private List<PotentialContractWithNonDevs> potentialContractWithNonDevs = new ArrayList<>();

    public GuildDev() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondFirstName() {
        return secondFirstName;
    }

    public void setSecondFirstName(String secondFirstName) {
        this.secondFirstName = secondFirstName;
    }

    public String getThirdFirstName() {
        return thirdFirstName;
    }

    public void setThirdFirstName(String thirdFirstName) {
        this.thirdFirstName = thirdFirstName;
    }

    public String getFourthFirstName() {
        return fourthFirstName;
    }

    public void setFourthFirstName(String fourthFirstName) {
        this.fourthFirstName = fourthFirstName;
    }

    public String getFifthFirstName() {
        return fifthFirstName;
    }

    public void setFifthFirstName(String fifthFirstName) {
        this.fifthFirstName = fifthFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GuildDev guildDev)) return false;

        if (!firstName.equals(guildDev.firstName)) return false;
        if (!Objects.equals(secondFirstName, guildDev.secondFirstName))
            return false;
        if (!Objects.equals(thirdFirstName, guildDev.thirdFirstName))
            return false;
        return lastName.equals(guildDev.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + (secondFirstName != null ? secondFirstName.hashCode() : 0);
        result = 31 * result + (thirdFirstName != null ? thirdFirstName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        return result;
    }
}

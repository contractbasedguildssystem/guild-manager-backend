package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Clause;

import javax.persistence.*;

@Entity
public class PotentialClause extends Clause {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_potential_clause_generator")
    @SequenceGenerator(name = "id_potential_clause_generator", sequenceName = "id_potential_clause_generator_sequence", allocationSize = 1)
    private Long id;

    @Column(nullable = false, unique = true, length = 2048)
    private String content;

    public PotentialClause() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PotentialClause that)) return false;
        if (!super.equals(o)) return false;

        return content.equals(that.content);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + content.hashCode();
        return result;
    }
}

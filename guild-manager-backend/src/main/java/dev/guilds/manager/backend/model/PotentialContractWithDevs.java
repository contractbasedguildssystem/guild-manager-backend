package dev.guilds.manager.backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PotentialContractWithDevs {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_potential_contract_with_devs_generator")
    @SequenceGenerator(name = "id_potential_contract_with_devs_generator", sequenceName = "id_potential_contract_with_devs_generator", allocationSize = 1)
    private Long id;

    private List<GuildDev> guildDevs = new ArrayList<>();

    private List<OtherGuildDev> otherGuildDevs = new ArrayList<>();

    private
}

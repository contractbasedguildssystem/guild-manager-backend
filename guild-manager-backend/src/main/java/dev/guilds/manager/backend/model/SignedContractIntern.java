package dev.guilds.manager.backend.model;

import dev.guilds.manager.backend.model.meta.Clause;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SignedContractIntern {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_signed_contract_intern_generator")
    @SequenceGenerator(name = "id_signed_contract_intern_generator", sequenceName = "id_signed_contract_intern_generator_sequence", allocationSize = 1)
    private Long id;

    @Column(updatable = false, nullable = false)
    @ManyToMany(mappedBy = "signedContractInterns")
    private List<GuildDev> signers = new ArrayList<>();

    @Column(updatable = false, nullable = false)
    @OneToMany(mappedBy = "signedContract")
    private List<Clause> clauses = new ArrayList<>();

}

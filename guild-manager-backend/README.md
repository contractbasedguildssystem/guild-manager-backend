# guild-manager-backend

## Code rules

* For all (guild-related) entities :
    * 1 rest resource injects 1 service and 1 mapper
    * 1 service injects 1 DAO and all services if it needs to.
    * 1 DAO injects 1 EntityManager (which is a Singleton)

* Non-public (package-protected) methods in Services means they are not supposed to be called by rest resources (but only by other services).

* For comments :

```java
/**
 * 
 * Your comment
 * 
 */
```

* Word "must" should be prohibited anywhere in code, config and comments.
  * Use "should" instead.

---

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

---

* Quarkus
  * Java
  * JDK 17
  * No native version planned
    * API
      * Impl
    * JAX-RS
      * ReastEasy Classic
    * JSON-B
      * Yasson
    * JPA
      * Hibernate
  * Use of mapStruct for rest resources DTO mappings 
* Postgres Database Management System (SGBDR)
